#pragma once

#include <iostream>
#include <unordered_map>
#include <string>
#include "redis.h"

class Cmd {
public:
    explicit Cmd(std::unordered_map<std::string, std::string>* um) : um_(um) {}
    virtual std::string name() = 0;
    virtual RedisValue exec(RedisValue args) = 0;

protected:
    std::unordered_map<std::string, std::string>* um_;

};

class Set : public Cmd {
public:
    explicit Set(std::unordered_map<std::string, std::string>* um) : Cmd(um) {}

    virtual std::string name() override;
    virtual RedisValue exec(RedisValue args) override;
};

class Get : public Cmd {
public:
    explicit Get(std::unordered_map<std::string, std::string>* um) : Cmd(um) {}

    virtual std::string name() override;
    virtual RedisValue exec(RedisValue args) override;
};

std::string bulk_to_string(RedisValue v);
RedisValue string_to_bulk(std::string str);