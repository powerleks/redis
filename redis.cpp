#include "redis.h"

void WriteRedisValue(Writer* w, const RedisValue& value) {
    if (value.which() == REDIS_INT) {
        w->write_char(':');
        w->write_int(boost::get<int64_t>(value));
        w->write_crlf();
    } else if (value.which() == REDIS_STRING) {
        w->write_char('+');
        w->write_string(boost::get<std::string>(value));
        w->write_crlf();
    } else if (value.which() == REDIS_BULK_STRING) {
        w->write_char('$');
        std::vector<char> tmp = boost::get<std::vector<char>>(value);
        w->write_int(int64_t (tmp.size()));
        w->write_crlf();
        if (tmp.size()) {
            w->write_raw(&tmp[0], tmp.size());
        }
        w->write_crlf();
    } else if (value.which() == REDIS_ERROR) {
        w->write_char('-');
        w->write_string((boost::get<RedisError>(value)).msg);
        w->write_crlf();
    } else if (value.which() == REDIS_ARRAY) {
        w->write_char('*');
        std::vector<RedisValue> v = boost::get<std::vector<RedisValue>>(value);;
        w->write_int(int64_t (v.size()));
        w->write_crlf();
        for (size_t i = 0; i < v.size(); ++i) {
            WriteRedisValue(w, v[i]);
        }        
        w->write_crlf();
    } else if (value.which() == REDIS_NULL) {
        w->write_char('$');
        w->write_int(-1);
        w->write_crlf();
    } else {
        throw std::runtime_error("unsupported type");
    }
}

void ReadRedisValue(Reader* r, RedisValue* value) {
    switch(r->read_char()) {
        case ':': {
            *value = r->read_int();
            break;
        }
        case '+': {
            *value = r->read_line();
            break;
        }
        case '$': {
            size_t len = r->read_int();
            if (len < 0) {
                *value = RedisNull();
                break;
            }
            *value = r->read_raw(len);
            break;
        }
        case '-': {
            std::string tmp = r->read_line();            
            *value = RedisError(tmp);
            break;
        }
        case '*': {
            size_t len = r->read_int();

            std::vector<RedisValue> v(len);
            
            for (size_t i = 0; i < len; ++i) {
                ReadRedisValue(r, &(v[i]));
            }
            *value = v;
            break;
        }
        default:
            throw std::runtime_error("invalid redis value");
    }
}