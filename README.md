#Redis-server
Покоев Алексей, группа 146

## О проекте
redis - это популярная in-memory key-value база данных.

## Что реализовано.
В проекте реализованы парсер, сервер, принимающий команды, и обработчик команд SET и GET.

## Парсер
Парсер умеет работать со следующими типами данных:

1. Simple String
2. Error
3. Integer
4. Bulk String
5. Array
6. Redis NUll

В парсере используется библиотека boost/variant.hpp, с помощью которой реализован тип Array.

Во время обработки программа считывает данные из буфера. Если в буфере записаны не все данные, то она вызывает метод read_more(), который записывает в буфер новые данные или кидает ошибку, если их нет.

## Сервер
Сервер умеет работать с несколькми клиентами, но одновременно может принимать команды только от одного. 

Сначала сервер устанавливает адрес, ко которому должны подключаться клиенты: 127.0.0.1:6380, а после этого переходит в режим listen и ждет подключениий. Сервер принимает подключение от одного клиента и, пока соединение не будет разорвано, отвечает ошибкой на попытки подключиться другими клиентами.

Сервер понимает только две команды SET и GET, на остальные команды он отвечает ошибкой. Для записи данных используется контейнер unordered_map. Данные в контейнере хранятся всё время, когда работает сервер и другие клиенты могут получать данные, которые записали предыдущие клиенты

## Обработчик команд
Обработчик команд умеет работать с двумя командами: SET, GET.
Он принимает от клиента команды в соотвествие с протоколом (клиент отправляет серверу тип Array, со значениями типа Bulk String).

Команды должны подаваться клиентом в следующем виде:

Set key value

Get value

Обработчик посылает RedisError с текстом ошибки в следующих случаях:

1. Поступила неизвестная команда
2. Неправильное колиество аргументов в одной из команд SET или GET.

В случае, если поступила комнада с несуществующим ключом, то обработчик посылает тип RedisNull.

В остальных случаях, обработчик команд, корректно обрабатывает команды и выполняет соотвествующее команде действие (для SET возвращает тип Bulk String с текстом "ОК", для GET - тип Bulk String с нужным значением).

## Чему научился
В ходе работы я познакомился с основами ООП, научился собирать код (с помщью cmake), тестировать его (с помощью библиотеки gtest), работать с сокетами.