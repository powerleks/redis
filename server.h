#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <string>
#include <string.h>
#include <errno.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <sys/wait.h>
#include "command.h"
#include "redis.h"

class Server {
public:
	Server(std::unordered_map<std::string, std::string> um, int port = 6380) : um_(um), port_(port) {};
	void start();
    void set_port(const int new_port);
    int create_descriptor();
    void establish_connection(const int& fd);
    void work();

protected:
	int port_;
	int afd;    
    std::unordered_map<std::string, std::string> um_;
};

RedisValue apply_cmd(const std::vector<Cmd*>& cmds, RedisValue req);