#include "reader.h"

char Reader::read_char() {
    if (rpos_ == end_) read_more();
    return buffer_[rpos_++];
}

int64_t Reader::read_int() {
    int64_t i = 0;
    bool negative = false;

    char first = read_char(), next;
    if (first == '-') {
        negative = true;
        next = read_char();
    } else {
        next = first;
    }

    do {
        i *= 10;
        if (next - '0' < 0 || next - '0' > 9) {
            std::cout << next << '\n';
            throw std::runtime_error("input string was not in a correct format");
        }
        i += next - '0';

        next = read_char();
    } while(next != '\r');
    read_char(); // skip '\n'

    return negative ? -i : i;
}

std::string Reader::read_line() { // read until "\r\n" and skip it
    std::string str;

    char next = read_char();

    while(next != '\r' && str.size() <= 1024) {
        if (next == '\n') {
            throw std::runtime_error("input string was not in a correct format");
        }

        str.push_back(next);

        next = read_char();
    }

    if (str.size() == 1024 && next != '\r') {
        throw std::runtime_error("too large size of string");
    }

    try {
        next = read_char();
    }
    catch (std::string str) {
        if (str == "end of input") {
            throw std::runtime_error("input string was not in a correct format");
        }
    }
    if (next != '\n') {
        throw std::runtime_error("input string was not in a correct format");
    }

    return str;
}

std::vector<char> Reader::read_raw(size_t len) {// read string with fixed size and skip "\r\n" at the end
    std::vector<char> str(len);

    char next = '0';

    size_t i;

    for (i = 0; i < len && next != '\r'; ++i) {
        next = read_char();

        if (next == '\n') {
            throw std::runtime_error("input string was not in a correct format");
        }

        str[i] = next;
    }
    
    if (i < len && next == '\r') {
        throw std::runtime_error("input string was not in a correct format");
    }

    read_char(); // skip '\r'
    read_char(); // skip '\n'

    return str;
}

void StringReader::read_more() {
    if (input.empty()) throw std::runtime_error("end of input");

    end_ = 0;
    rpos_ = 0;
    for (; end_ < input.size() && end_ < buffer_.size(); ++end_) {
        buffer_[end_] = input[end_];
    }

    input.erase(input.begin(), input.begin() + end_);
}

void SocketReader::read_more() {
    //if (input.empty()) throw std::runtime_error("end of input");

    end_ = 0;
    rpos_ = 0;

    end_ = read(afd_, &buffer_[0], buffer_.size());
    
    if (end_ < 0) {
        throw std::runtime_error("error while reading");
    }
}