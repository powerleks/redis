#include "command.h"

std::string bulk_to_string(RedisValue v) {
    std::vector<char> val = boost::get<std::vector<char>>(v);
    std::string str;
    for (size_t i = 0 ; i < val.size(); ++i) {
        str.push_back(val[i]);
    }
    return str;
}

RedisValue string_to_bulk(std::string str) {
    std::vector<char> val(str.size());
    for (size_t i = 0 ; i < str.size(); ++i) {
        val[i] = str[i];
    }
    return val;
}

std::string Set::name() {
    std::string cmd = "set";
    return cmd;
}

RedisValue Set::exec(RedisValue args) {
    std::vector<RedisValue> val = boost::get<std::vector<RedisValue>>(args);
    
    if (val.size() != 3) {
        return RedisError("ERR wrong number of arguments for 'set' command");
    }

    std::string key = bulk_to_string(val[1]);
    std::string value = bulk_to_string(val[2]);

    (*um_)[key] = value;

    return "OK";
}

std::string Get::name() {
    std::string cmd = "get";
    return cmd;
}

RedisValue Get::exec(RedisValue args) {
    std::vector<RedisValue> val = boost::get<std::vector<RedisValue>>(args); 

    if (val.size() != 2) {
        return RedisError("ERR wrong number of arguments for 'get' command");
    }

    std::string key = bulk_to_string(val[1]);

    std::unordered_map<std::string, std::string>::iterator it = (*um_).find(key);

    if (it == (*um_).end()) {
        return RedisNull();
    }

    return string_to_bulk(it->second);
}