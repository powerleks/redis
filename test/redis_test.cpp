#include "redis.h"

#include <gtest/gtest.h>

TEST(RedisValue, Construct) {
    RedisValue integer = 10;
    RedisValue string = "abcd";
    RedisValue error = RedisError("Permission denied");

    RedisValue array = std::vector<RedisValue>{integer, string, error};
}

TEST(WriteRedisValue, Int) {
    RedisValue i = 10, j = -5;

    StringWriter writer(1024);
    WriteRedisValue(&writer, i);
    writer.flush();

    EXPECT_STREQ(":10\r\n", writer.result.c_str());

    writer.result.clear();
    WriteRedisValue(&writer, j);
    writer.flush();

    EXPECT_STREQ(":-5\r\n", writer.result.c_str());
}

TEST(ReadRedisValue, Int) {
    RedisValue val;

    StringReader reader;

    reader.input = ":10\r\n";
    ReadRedisValue(&reader, &val);
    EXPECT_EQ(10, boost::get<int64_t>(val));

    reader.input = ":-50\r\n";
    ReadRedisValue(&reader, &val);
    EXPECT_EQ(-50, boost::get<int64_t>(val));
}

TEST(ReadRedisValue, Wrong_Int) {
    RedisValue val;

    StringReader reader;

    reader.input = ":26d\r\n";
    ReadRedisValue(&reader, &val);
    EXPECT_EQ(26, boost::get<int64_t>(val));
}

TEST(WriteRedisValue, String) {
    RedisValue i = "abc", j = "sample", k = "";

    StringWriter writer(1024);
    WriteRedisValue(&writer, i);
    writer.flush();

    EXPECT_STREQ("+abc\r\n", writer.result.c_str());

    writer.result.clear();
    WriteRedisValue(&writer, j);
    writer.flush();

    EXPECT_STREQ("+sample\r\n", writer.result.c_str());

    writer.result.clear();
    WriteRedisValue(&writer, k);
    writer.flush();

    EXPECT_STREQ("+\r\n", writer.result.c_str());
}

void CheckIncorrectInput(std::string input) {
    RedisValue val;
    StringReader reader;
    reader.input = input;
    //ASSERT_EQ(ReadRedisValue(&reader, &val), std::runtime_error);   
}

TEST(ReadRedisValue, Wrong_String) {
    RedisValue val;

    StringReader reader;

    /*reader.input = "+a\nc\r\n";
    ASSERT_EQ(ReadRedisValue(&reader, &val), std::runtime_error);

    EXPECT_EQ("a\nc", boost::get<std::string>(val));*/

    /*reader.input = "+samle\r\n";
    ReadRedisValue(&reader, &val);
    EXPECT_EQ("sale", boost::get<std::string>(val));*/

    reader.input = "+\n\r\n";
    ReadRedisValue(&reader, &val);
    EXPECT_EQ("", boost::get<std::string>(val));
    
    /*std::string tmp;
    for (size_t i = 0; i < 1025; ++i) {
        tmp.push_back('a');
    }
    reader.input = tmp;
    reader.input = "+\r\n";
    ReadRedisValue(&reader, &val);
    for (size_t i = 0; i < 1025; ++i) {
        EXPECT_EQ(tmp[i], boost::get<std::string>(val[i]));
    }*/
}

TEST(ReadRedisValue, String) {
    RedisValue val;

    StringReader reader;

    reader.input = "+abc\r\n";
    ReadRedisValue(&reader, &val);
    EXPECT_EQ("abc", boost::get<std::string>(val));

    reader.input = "+sample\r\n";
    ReadRedisValue(&reader, &val);
    EXPECT_EQ("sample", boost::get<std::string>(val));

    reader.input = "+\r\n";
    ReadRedisValue(&reader, &val);
    EXPECT_EQ("", boost::get<std::string>(val));
}

TEST(WriteRedisValue, Bulk_String) {
    std::vector<char> str;

    RedisValue j = str;
    
    std::string s = "foobar";
    str.resize(6);
    for (int i = 0; i < s.size(); ++i) {
        str[i] = s[i];
    }
    RedisValue i = str;

    StringWriter writer(1024);
    WriteRedisValue(&writer, i);
    writer.flush();

    EXPECT_STREQ("$6\r\nfoobar\r\n", writer.result.c_str());

    writer.result.clear();
    WriteRedisValue(&writer, j);
    writer.flush();

    EXPECT_STREQ("$0\r\n\r\n", writer.result.c_str());
}

TEST(ReadRedisValue, Bulk_String) {
    RedisValue val;

    StringReader reader;

    std::string str = "foobar";

    reader.input = "$6\r\nfoobar\r\n";
    ReadRedisValue(&reader, &val);
    std::vector<char> tmp = boost::get<std::vector<char>>(val);
    ASSERT_EQ(6, str.size());
    for (int i = 0; i < 6; ++i) {
        EXPECT_EQ(str[i], tmp[i]);
    }

    /*reader.input = "$0\r\n\r\n";
    ReadRedisValue(&reader, &val);
    EXPECT_EQ("", boost::get<std::vector<char>>(val));*/
}

TEST(WriteRedisValue, Error) {
    RedisValue i = RedisError("Error message"), 
        j = RedisError("WRONGTYPE Operation against a key holding the wrong kind of value");

    StringWriter writer(1024);
    WriteRedisValue(&writer, i);
    writer.flush();

    EXPECT_STREQ("-Error message\r\n", writer.result.c_str());

    writer.result.clear();
    WriteRedisValue(&writer, j);
    writer.flush();

    EXPECT_STREQ("-WRONGTYPE Operation against a key holding the wrong kind of value\r\n", writer.result.c_str());
}

TEST(ReadRedisValue, Error) {
    RedisValue val;

    StringReader reader;

    reader.input = "-Error message\r\n";
    ReadRedisValue(&reader, &val);
    EXPECT_STREQ("Error message", boost::get<RedisError>(val).msg.c_str());

    reader.input = "-ERR unknown command 'foobar'\r\n";
    ReadRedisValue(&reader, &val);
    EXPECT_STREQ("ERR unknown command 'foobar'", boost::get<RedisError>(val).msg.c_str());
}

TEST(ReadRedisValue, Array) {
    RedisValue val;

    StringReader reader;

    reader.input = "*5\r\n:1\r\n+OK\r\n-Error message\r\n$6\r\nfoobar\r\n:5\r\n";
    ReadRedisValue(&reader, &val);

    std::vector<RedisValue> v = boost::get<std::vector<RedisValue>>(val);
    
    ASSERT_EQ(5, v.size());
    EXPECT_EQ(1, boost::get<int64_t>(v[0]));
    EXPECT_EQ("OK", boost::get<std::string>(v[1]));
    EXPECT_STREQ("Error message", boost::get<RedisError>(v[2]).msg.c_str());
    std::vector<char> tmp = boost::get<std::vector<char>>(v[3]);
    std::string str = "foobar";
    ASSERT_EQ(6, str.size());
    for (int i = 0; i < 6; ++i) {
        EXPECT_EQ(str[i], tmp[i]);
    }
    EXPECT_EQ(5, boost::get<int64_t>(v[4]));
}
