#include "server.h"

void Server::start() {
    while (1) {
        int fd = create_descriptor();
        establish_connection(fd);
        try {
            work();
        }
        catch (std::runtime_error) {
            continue;
        }
    }
}

void Server::set_port(const int new_port) {
    if (new_port < 0 || new_port > 65535) {
        fprintf(stderr, "invalid port\n");
        return;
    }

    port_ = new_port;
}

int Server::create_descriptor() {
    int fd = socket(PF_INET, SOCK_STREAM, 0);
    if (fd < 0) {
        fprintf(stderr, "socket failed: %s\n", strerror(errno));
        return 1;
    }

    int opt = 1;
    setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(opt));

    struct sockaddr_in ba;
    ba.sin_family = AF_INET;
    ba.sin_port = htons(port_);
    ba.sin_addr.s_addr = INADDR_ANY;
    if (bind(fd, (struct sockaddr*) &ba, sizeof(ba)) < 0) {
        fprintf(stderr, "bind failed: %s\n", strerror(errno));
        return 1;
    }

    if (listen(fd, 10) < 0) {
        fprintf(stderr, "listen failed: %s\n", strerror(errno));
        return 1;
    }

    return fd;
} 

void Server::establish_connection(const int& fd) {
    struct sockaddr_in aa;
    socklen_t slen = sizeof(aa);
    afd = accept(fd, (struct sockaddr *) &aa, &slen);
    if (afd < 0) {
        fprintf(stderr, "accept failed: %s\n", strerror(errno));
        return;
    }
    close(fd);   
}

RedisValue apply_cmd(const std::vector<Cmd*>& cmds, RedisValue req) {
    std::vector<RedisValue> val = boost::get<std::vector<RedisValue>>(req);        
    std::string cmd = bulk_to_string(val[0]);

    for (size_t i = 0; i < cmds.size(); ++i) {
        Cmd* p = cmds[i];
        if (p->name() == cmd) {
            return p->exec(req);
        }
    }
    std::string ans = "ERR unknow command '";
    ans += cmd;
    ans += "'";
    return RedisError(ans);
}

void Server::work() {
    SocketWriter w(afd);

    Set set_cmd(&um_);
    Get get_cmd(&um_);

    std::vector<Cmd*> l = { &set_cmd, &get_cmd };

    while (1) {
        SocketReader r(afd);
        RedisValue val;
        ReadRedisValue(&r, &val);
        RedisValue rsp = apply_cmd(l, val);
        WriteRedisValue(&w, rsp);
        w.flush();
    }
}