#include <iostream>

#include "redis.h"
#include "server.h"

int main() {    
    std::unordered_map<std::string, std::string> um;
	Server s(um);
    s.start();
    return 0;
}